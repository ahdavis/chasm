/*
 * AddressTable.cpp
 * Implements a class that represents an address table
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include
#include "AddressTable.h"

//class implementation

//constructor
AddressTable::AddressTable()
	: size(0), data() //init the fields
{
	//no code needed
}

//destructor
AddressTable::~AddressTable() {
	this->size = 0; //set the size to 0
}

//copy constructor
AddressTable::AddressTable(const AddressTable& at)
	: size(at.size), data(at.data) //copy the fields
{
	//no code needed
}

//assignment operator
AddressTable& AddressTable::operator=(const AddressTable& src) {
	this->size = src.size; //assign the size field
	this->data = src.data; //assign the data field
	return *this; //and return the instance
}

//getSize method - returns the number of entries in the table
int AddressTable::getSize() const {
	return this->size; //return the size field
}

//addEntry method - adds an entry to the table
void AddressTable::addEntry(const std::string& label,
				std::uint16_t addr) {
	//make sure that the table does not already have
	//an entry for the given label
	if(!this->hasEntry(label)) {
		this->data[label] = addr; //add the address to the table
		this->size++; //and increment the size field
	}
}

//hasEntry method - returns whether the table has an entry for a label
bool AddressTable::hasEntry(const std::string& label) const {
	return this->data.find(label) != this->data.end();
}

//getEntry method - returns the address corresponding to a label
std::uint16_t AddressTable::getEntry(const std::string& label) const {
	return this->data.at(label);
}

//end of implementation
