/*
 * AddressTable.h
 * Declares a class that represents an address table
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <map>
#include <cstdint>

//class declaration
class AddressTable {
	//public fields and methods
	public:
		//constructor
		AddressTable();

		//destructor
		virtual ~AddressTable();

		//copy constructor
		AddressTable(const AddressTable& at);

		//assignment operator
		AddressTable& operator=(const AddressTable& src);

		//getter method
		
		//returns the number of entries in the table
		int getSize() const;

		//other methods
		
		//adds an address and label to the table
		void addEntry(const std::string& label, 
				std::uint16_t addr);

		//returns whether a label has an entry in the table
		bool hasEntry(const std::string& label) const;

		//returns the address corresponding to a label
		std::uint16_t getEntry(const std::string& label) const;

	//protected fields and methods
	protected:
		//fields
		int size; //the number of entries in the table
		std::map<std::string, uint16_t> data; //the table data
};

//end of header
