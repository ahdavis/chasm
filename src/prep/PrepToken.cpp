/*
 * PrepToken.cpp
 * Implements a class that represents a preprocessor token
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include
#include "PrepToken.h"

//class implementation

//first constructor - constructs from only a token type
PrepToken::PrepToken(PrepTokenType newType)
	: PrepToken(newType, "") //call the second constructor
{
	//no code needed
}

//second constructor - constructs from a type and a value
PrepToken::PrepToken(PrepTokenType newType, const std::string& newValue)
	: type(newType), value(newValue) //init the fields
{
	//no code needed
}

//destructor
PrepToken::~PrepToken() {
	//no code needed
}

//copy constructor
PrepToken::PrepToken(const PrepToken& pt)
	: type(pt.type), value(pt.value) //copy the fields
{
	//no code needed
}

//assignment operator
PrepToken& PrepToken::operator=(const PrepToken& src) {
	this->type = src.type; //assign the type field
	this->value = src.value; //assign the value field
	return *this; //and return the instance
}

//getType method - returns the type of the token
PrepTokenType PrepToken::getType() const {
	return this->type; //return the type field
}

//getValue method - returns the value of the token
const std::string& PrepToken::getValue() const {
	return this->value; //return the value field
}

//end of implementation
