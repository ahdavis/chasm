/*
 * PrepLexer.cpp
 * Declares a class that lexes preprocessor symbols
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "PrepLexer.h"
#include <cstdlib>
#include "PrepTokenType.h"
#include "../except/PrepLexerException.h"
#include <cctype>

//class implementation

//constructor
PrepLexer::PrepLexer(const std::string& newText)
	: text(newText), curChar(' '), pos(0), addr(0x200), nibCount(0)
{
	//set the current character
	this->curChar = this->text[this->pos];
}

//destructor
PrepLexer::~PrepLexer() {
	//no code needed
}

//copy constructor
PrepLexer::PrepLexer(const PrepLexer& pl)
	: text(pl.text), curChar(pl.curChar), pos(pl.pos), addr(pl.addr),
		nibCount(pl.nibCount)
{
	//no code needed
}

//assignment operator
PrepLexer& PrepLexer::operator=(const PrepLexer& src) {
	this->text = src.text; //assign the text field
	this->curChar = src.curChar; //assign the current character field
	this->pos = src.pos; //assign the position field
	this->addr = src.addr; //assign the address field
	this->nibCount = src.nibCount; //assign the nibble count
	return *this; //and return the instance
}

//getAddress method - returns the current address being processed
std::uint16_t PrepLexer::getAddress() const {
	return this->addr; //return the address field
}

//getNextToken method - returns the next token consumed from the text
PrepToken PrepLexer::getNextToken() {
	//loop and process the text
	while(this->curChar != '\0') {
		//process whitespace
		if(std::isspace(this->curChar)) {
			this->skipWhitespace();
			continue;
		}

		//process commas
		if(this->curChar == ',') {
			this->advance(); 
			continue;
		}

		//process periods
		if(this->curChar == '.') {
			this->advance();
			continue;
		}

		//process comments
		if(this->curChar == ';') {
			this->consumeComment();
			continue;
		}

		//process register references
		if((this->curChar == 'V') ||
			(this->curChar == 'I')){
			this->consumeRegister();
			this->updateAddr();
			continue;
		}

		//process opcode references
		if(std::isalpha(this->curChar)) {
			this->consumeOp();
			this->updateAddr();
			continue;
		}

		//process integer literals
		if(this->curChar == '$') {
			this->consumeLiteral();
			this->updateAddr();
			continue;
		}

		//process binary literals
		if(this->curChar == '%') {
			this->consumeBinary();
			this->updateAddr();
			continue;
		}

		//process labels
		if(this->curChar == '_') {
			//get the label text
			std::string lbl = this->consumeLabel();

			//determine whether it's a definition
			char last = lbl.at(lbl.length() - 1);
			if(last == ':') {
				std::string tlbl = lbl.substr(0, 
							lbl.length() - 1);
				return PrepToken(PrepTokenType::LABELD,
							tlbl);
			} else {
				this->nibCount += 2;
				this->updateAddr();
				return PrepToken(PrepTokenType::LABEL,
							lbl);
			}
		}

		//if control reaches here, then an unknown character
		//was found, so we throw an exception
		throw PrepLexerException(this->curChar);
	}

	//and return an EOF token
	return PrepToken(PrepTokenType::END);
}

//private advance method - advances the lexer position
void PrepLexer::advance() {
	this->pos++; //increment the position counter

	//and handle end of text
	if(this->pos > (this->text.length() - 1)) {
		this->curChar = '\0';
	} else {
		this->curChar = this->text[this->pos];
	}
}

//private skipWhitespace method - skips whitespace in the text
void PrepLexer::skipWhitespace() {
	//loop through the whitespace
	while((this->curChar != '\0') && std::isspace(this->curChar)) {
		this->advance();
	}
}

//private updateAddr method - updates the address field
void PrepLexer::updateAddr() {
	if(this->nibCount >= 4) {
		this->nibCount = 0;
		this->addr += 2;
	}
}

//private consumeRegister method - consumes a register reference
void PrepLexer::consumeRegister() {
	if(this->curChar != 'I') {
		this->advance(); //advance through the V
	}
	this->advance(); //advance through the index
	this->nibCount++; //and increase the nibble count
}

//private consumeOp method - consumes an opcode
void PrepLexer::consumeOp() {
	std::string op;
	while((std::isalpha(this->curChar) || (this->curChar == '.')) 
		&& !std::isspace(this->curChar)) {
		op += this->curChar;
		this->advance();
	}

	//handle incrementing the nibble count
	//CLS (00E0) and RET (00EE) are the only 2-byte instructions
	if((op == "CLS") || (op == "RET")) { 
		this->nibCount += 4;
	} else {
		this->nibCount += 2;
	}
}

//private consumeLiteral method - consumes an integer literal
void PrepLexer::consumeLiteral() {
	this->advance(); //advance past the opening $

	//loop through the digits
	int nibsAdvanced = 0; //the number of digits processed
	while(std::isxdigit(this->curChar)) {
		nibsAdvanced++;
		this->advance();
	}

	//and advance the nibble count
	this->nibCount += nibsAdvanced;
}

//private consumeBinary method - consumes a binary literal
void PrepLexer::consumeBinary() {
	this->advance(); //advance past the opening %
	
	//loop through the binary
	for(int i = 0; i < 8; i++) {
		this->advance();
	}

	//and advance the nibble count
	this->nibCount += 2;
}

//private consumeLabel method - consumes a label
std::string PrepLexer::consumeLabel() {
	std::string ret = "_"; //will hold the label
	this->advance(); //advance the lexer
	
	//loop through the label
	while(std::isalpha(this->curChar) && 
		!std::isspace(this->curChar)) {
		ret += this->curChar;
		this->advance();
	}

	//check for a colon
	if(this->curChar == ':') {
		ret += ':';
		this->advance();
	}

	//and return the label
	return ret;
}

//private consumeComment method - consumes a comment
void PrepLexer::consumeComment() {
	//loop to the end of the line
	while((this->curChar != '\n') && (this->curChar != '\0')) {
		this->advance();
	}

	//and advance past the newline
	this->advance();
}

//end of implementation
