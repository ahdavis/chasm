/*
 * Preprocessor.cpp
 * Implements a class that preprocesses text
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include
#include "Preprocessor.h"
#include "PrepTokenType.h"

//class implementation

//constructor
Preprocessor::Preprocessor(const PrepLexer& newLexer)
	: lexer(newLexer), curToken(PrepTokenType::END), addrs()
{
	//get the first token
	this->curToken = this->lexer.getNextToken();

	//add it to the address table
	if(this->curToken.getType() != PrepTokenType::END) {
		this->addrs.addEntry(this->curToken.getValue(),
					this->lexer.getAddress());
	}
}

//destructor
Preprocessor::~Preprocessor() {
	//no code needed
}

//copy constructor
Preprocessor::Preprocessor(const Preprocessor& p)
	: lexer(p.lexer), curToken(p.curToken), addrs(p.addrs)
{
	//no code needed
}

//assignment operator
Preprocessor& Preprocessor::operator=(const Preprocessor& src) {
	this->lexer = src.lexer; //assign the lexer
	this->curToken = src.curToken; //assign the current token
	this->addrs = src.addrs; //assign the address table
	return *this; //and return the instance
}

//process method - preprocesses text and returns a table of label addresses
const AddressTable& Preprocessor::process() {
	//loop and preprocess the text
	do {
		//add the current token to the address table 
		//if it is not an EOF or label reference token
		if((this->curToken.getType() != PrepTokenType::END)
			&& (this->curToken.getType() != 
				PrepTokenType::LABEL)) {
			this->addrs.addEntry(this->curToken.getValue(),
						this->lexer.getAddress());
		}

		//and get the next token
		this->curToken = this->lexer.getNextToken();

	} while(this->curToken.getType() != PrepTokenType::END);

	//and return the completed address table
	return this->addrs;
}

//end of implementation
