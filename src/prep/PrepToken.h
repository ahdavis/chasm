/*
 * PrepToken.h
 * Declares a class that represents a preprocessor token
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "PrepTokenType.h"
#include <string>

//class declarations
class PrepToken {
	//public fields and methods
	public:
		//constructs from only a token type
		explicit PrepToken(PrepTokenType newType);

		//constructs from a token type and a value
		PrepToken(PrepTokenType newType, 
				const std::string& newValue);

		//destructor
		virtual ~PrepToken();

		//copy constructor
		PrepToken(const PrepToken& pt);

		//assignment operator
		PrepToken& operator=(const PrepToken& src);

		//getter methods
		
		//returns the type of the token
		PrepTokenType getType() const;

		//returns the value of the token
		const std::string& getValue() const;

	//protected fields and methods
	protected:
		//fields
		PrepTokenType type; //the type of the token
		std::string value; //the value of the token
};

//end of header
