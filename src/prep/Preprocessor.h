/*
 * Preprocessor.h
 * Declares a class that preprocesses text
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "AddressTable.h"
#include "PrepLexer.h"
#include "PrepToken.h"

//class declaration
class Preprocessor {
	//public fields and methods
	public:
		//constructor
		explicit Preprocessor(const PrepLexer& newLexer);

		//destructor
		virtual ~Preprocessor();

		//copy constructor
		Preprocessor(const Preprocessor& p);

		//assignment operator
		Preprocessor& operator=(const Preprocessor& src);

		//processes text and returns a table of address labels
		const AddressTable& process();

	//protected fields and methods
	protected:
		//field
		PrepLexer lexer; //the text tokenizer
		PrepToken curToken; //the current token being processed
		AddressTable addrs; //the table of addresses to generate
};

//end of header
