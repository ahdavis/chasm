/*
 * PrepLexer.h
 * Declares a class that lexes preprocessor symbols
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include <cstdint>
#include "PrepToken.h"

//class declaration
class PrepLexer {
	//public fields and methods
	public:
		//constructor
		explicit PrepLexer(const std::string& text);

		//destructor
		virtual ~PrepLexer();

		//copy constructor
		PrepLexer(const PrepLexer& pl);

		//assignment operator
		PrepLexer& operator=(const PrepLexer& src);

		//getter methods
		
		//returns the current memory address 
		//being preprocessed at
		std::uint16_t getAddress() const;

		//returns the next token consumed from the text
		PrepToken getNextToken();

	//protected fields and methods
	protected:
		//methods
		void advance(); //advances the lexer to the next character
		void skipWhitespace(); //skips whitespace in the text
		void updateAddr(); //updates the address field
		void consumeRegister(); //consumes a register in the text
		void consumeOp(); //consumes an opcode in the text
		void consumeLiteral(); //consumes an integer literal
		void consumeBinary(); //consumes a binary literal
		std::string consumeLabel(); //consumes a label
		void consumeComment(); //consumes a comment

		//fields
		std::string text; //the text being lexed
		char curChar; //the current character being processed
		unsigned int pos; //the current position in the text
		std::uint16_t addr; //the RAM address for creating labels
		int nibCount; //the number of nibbles processed
};

//end of header
