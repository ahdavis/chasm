/*
 * Opcode.cpp
 * Implements a class that represents an opcode
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <sstream>
#include <cctype>
#include "Opcode.h"
#include "../except/NameException.h"

//class implementation

//constructor
Opcode::Opcode()
	: opStr() //init the field
{
	//no code needed
}

//destructor
Opcode::~Opcode() {
	//no code needed
}

//copy constructor
Opcode::Opcode(const Opcode& op)
	: opStr(op.opStr) //copy the field
{
	//no code needed
}

//assignment operator
Opcode& Opcode::operator=(const Opcode& src) {
	this->opStr = src.opStr; //assign the opcode string
	return *this; //and return the instance
}

//getCode method - returns the string representing the opcode
const std::string& Opcode::getCode() const {
	return this->opStr; //return the opcode string
}

//getValue method - returns the numeric value of the opcode
std::uint16_t Opcode::getValue() const {
	//declare variables
	std::stringstream ss; //used to convert the string to an opcode
	std::uint16_t ret = 0; //will hold the numeric opcode

	//stream in the opcode string
	ss << std::hex << this->opStr;

	//stream it out into the return value
	ss >> ret;

	//and return the return value
	return ret;
}

//getName method - returns the name of the opcode
OpName Opcode::getName() const {
	//get the value of the opcode
	std::uint16_t value = this->getValue();

	//declare the return value
	OpName ret;

	//determine the name of the opcode
	switch(value & 0xF000) {
		//0XXX - CLS and RET
		case 0x0000: {
			switch(value & 0x000F) {
				case 0x0000: { //CLS
					ret = OpName::CLS;
					break;
				}

				case 0x000E: { //RET
					ret = OpName::RET;
					break;
				}

				default: { //unknown opcode
					throw NameException(*this);
				}
			}

			break;
		}

		case 0x1000: { //JP
			ret = OpName::JP;
			break;
		}

		case 0x2000: { //CALL
			ret = OpName::CALL;
			break;
		}

		case 0x3000: { //SEC
			ret = OpName::SEC;
			break;
		}

		case 0x4000: { //SNEC
			ret = OpName::SNEC;
			break;
		}

		case 0x5000: { //SKRE
			ret = OpName::SKRE;
			break;
		}

		case 0x6000: { //SETC
			ret = OpName::SETC;
			break;
		}

		case 0x7000: { //ADDC
			ret = OpName::ADDC;
			break;
		}

		//8XXX: Register operations
		case 0x8000: { 
			switch(value & 0x000F) {
				case 0x0000: { //SETR
					ret = OpName::SETR;
					break;
				}
				
				case 0x0001: { //OR
					ret = OpName::OR;
					break;
				}

				case 0x0002: { //AND
					ret = OpName::AND;
					break;
				}

				case 0x0003: { //XOR
					ret = OpName::XOR;
					break;
				}

				case 0x0004: { //ADDR
					ret = OpName::ADDR;
					break;
				}

				case 0x0005: { //SUB
					ret = OpName::SUB;
					break;
				}

				case 0x0006: { //RSH
					ret = OpName::RSH;
					break;
				}

				case 0x0007: { //SUBB
					ret = OpName::SUBB;
					break;
				}

				case 0x000E: { //LSH
					ret = OpName::LSH;
					break;
				}

				default: { //unknown opcode
					throw NameException(*this);
				}

			}

			break;
		}

		case 0x9000: { //SKRN
			ret = OpName::SKRN;
			break;	
		}

		case 0xA000: { //SETI
			ret = OpName::SETI;
			break;
		}

		case 0xB000: { //JPI
			ret = OpName::JPI;
			break;
		}

		case 0xC000: { //RAND
			ret = OpName::RAND;
			break;
		}

		case 0xD000: { //DRAW
			ret = OpName::DRAW;
			break;
		}

		//EXXX: Key skips
		case 0xE000: {
			switch(value & 0x00FF) {
				case 0x009E: { //SKEQ
					ret = OpName::SKEQ;
					break;
				}

				case 0x00A1: { //SKNE
					ret = OpName::SKNE;
					break;
				}

				default: { //unknown opcode
					throw NameException(*this);
				}
			}

			break;
		}

		//FXXX: Miscellaneous operations
		case 0xF000: {
			switch(value & 0x00FF) {
				case 0x0007: { //TIME
					ret = OpName::TIME;
					break;
				}

				case 0x000A: { //WKEY
					ret = OpName::WKEY;
					break;
				}

				case 0x0015: { //DELY
					ret = OpName::DELY;
					break;
				}

				case 0x0018: { //SND
					ret = OpName::SND;
					break;
				}

				case 0x001E: { //ADDI
					ret = OpName::ADDI;
					break;
				}

				case 0x0029: { //SPRC
					ret = OpName::SPRC;
					break;
				}

				case 0x0033: { //BCD
					ret = OpName::BCD;
					break;
				}

				case 0x0055: { //RDMP
					ret = OpName::RDMP;
					break;
				}

				case 0x0065: { //RLD
					ret = OpName::RLD;
					break;
				}

				default: { //unknown opcode
					throw NameException(*this);
				}
			}

			break;
		}

		default: { //unknown opcode
			throw NameException(*this);
		}
	}

	//and return the calculated name
	return ret;
}

//appendNibble method - appends a nibble to the opcode
void Opcode::appendNibble(char nibble) {
	//make sure that the opcode is not already filled with data
	if(this->opStr.length() <= 4) {
		//make sure that the nibble is a hex digit
		if(std::isxdigit(nibble)) {
			//append the nibble to the string
			this->opStr += nibble;
		}
	}
}

//output operator
std::ostream& operator<<(std::ostream& os, const Opcode& op) {
	os << op.getValue(); //stream out the numeric value of the opcode
	return os; //and return the stream
}

//end of implementation
