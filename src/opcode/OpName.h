/*
 * OpName.h
 * Enumerates opcode names
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//no includes

//enum definition
enum class OpName {
	CLS,  //00E0: Clears the screen
	RET,  //00EE: Return from subroutine
	JP,   //1NNN: Jump to address NNN
	CALL, //2NNN: Call subroutine at address NNN
	SEC,   //3XNN: Skip next instruction if VX equals NN
	SNEC, //4XNN: Skip next instruction if VX does not equal NN
	SKRE, //5XY0: Skip next instruction if VX equals VY
	SETC, //6XNN: Set VX to NN
	ADDC, //7XNN: Add NN to VX
	SETR, //8XY0: Set VX to VY
	OR,   //8XY1: VX |= VY
	AND,  //8XY2: VX &= VY
	XOR,  //8XY3: VX ^= VY
	ADDR, //8XY4: VX += VY
	SUB,  //8XY5: VX -= VY
	RSH,  //8XY6: VX >>= 1
	SUBB, //8XY7: VX = VY - VX
	LSH,  //8XYE: VX <<= 1
	SKRN, //9XY0: Skip next instruction if VX does not equal VY
	SETI, //ANNN: Set I to NNN
	JPI,  //BNNN: Jump to V0 + NNN
	RAND, //CXNN: Set VX to a random number AND NN
	DRAW, //DXYN: Draw a sprite at VX, VX with a height of N
	SKEQ, //EX9E: Skip next instruction if key VX is pressed
	SKNE, //EXA1: Skip next instruction if key VX is not pressed
	TIME, //FX07: Set VX to the delay timer
	WKEY, //FX0A: Wait for a keypress and store it in VX
	DELY, //FX15: Set the delay timer to VX
	SND,  //FX18: Set the sound timer to VX
	ADDI, //FX1E: Add VX to I
	SPRC, //FX29: Set I to the address of the character in VX
	BCD,  //FX33: Store the BCD version of VX in memory at I
	RDMP, //FX55: Store V0 to VX in memory at I
	RLD   //FX65: Read memory starting at I into V0 through VX
};

//end of header
