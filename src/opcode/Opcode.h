/*
 * Opcode.h
 * Declares a class that represents an opcode
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <cstdint>
#include "OpName.h"

//class declaration
class Opcode {
	//public fields and methods
	public:
		//constructor
		Opcode();

		//destructor
		virtual ~Opcode();

		//copy constructor
		Opcode(const Opcode& op);

		//assignment operator
		Opcode& operator=(const Opcode& src);

		//getter methods
		
		//returns the string value of the Opcode
		const std::string& getCode() const;

		//returns the numeric value of the Opcode
		std::uint16_t getValue() const;

		//returns the name of the Opcode
		OpName getName() const;

		//other methods
		
		//adds a hex digit to the Opcode
		void appendNibble(char nibble);

		//stream operator
		friend std::ostream& operator<<(std::ostream& os, 
							const Opcode& op);

	//protected fields and methods
	protected:
		//field
		std::string opStr; //the string that makes up the opcode
};

//end of header
