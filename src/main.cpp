/*
 * main.cpp
 * Entry point for chasm
 * Created on 4/28/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <streambuf>
#include <vector>
#include <cstdint>
#include "util/fileExists.h"
#include "util/trimFilename.h"
#include "util/swapEndian.h"
#include "codegen/Lexer.h"
#include "codegen/Assembler.h"
#include "prep/AddressTable.h"
#include "prep/Preprocessor.h"
#include "prep/PrepLexer.h"
#include "except/AsmException.h"
#include "except/DeviceException.h"
#include "except/LabelException.h"
#include "except/LexerException.h"
#include "except/PrepLexerException.h"

//main function - main entry point for the program
int main(int argc, char* argv[]) {
	//make sure an argument was supplied
	if(argc != 2) {
		//display an error message
		std::cout << "Usage: " << argv[0] 
			  << " <filename>" << std::endl;

		//and exit with an error
		return EXIT_FAILURE;
	}

	//make sure the file in the argument exists
	if(!fileExists(argv[1])) {
		//display an error message
		std::cout << "Could not open " << argv[1] << std::endl;

		//and exit with an error
		return EXIT_FAILURE;
	}

	//calculate the target filename
	std::string target = trimFilename(argv[1]);
	target += ".c8";

	//make sure the target file doesn't already exist
	if(fileExists(target)) {
		std::cout << "Target binary " << target 
			  << " already exists" << std::endl;
		return EXIT_FAILURE;
	}

	//open the source file and read it into a string
	std::ifstream ifs(argv[1]);
	std::string code;
	ifs.seekg(0, std::ios::end);
	code.reserve(ifs.tellg());
	ifs.seekg(0, std::ios::beg);
	code.assign((std::istreambuf_iterator<char>(ifs)),
			std::istreambuf_iterator<char>());
	ifs.close();

	//actually assemble the program
	try {
		//first, we preprocess the program
		PrepLexer plex(code);
		Preprocessor prep(plex);
		AddressTable addrs = prep.process();		

		//then we assemble the program
		Lexer lex(code);
		Assembler asmb(lex, addrs);
		std::vector<std::uint16_t> prog = asmb.assemble();

		//and finally, we write the assembled
		//program to a file
		std::ofstream ofs(target, std::ios::binary);
		for(const auto& op : prog) {
			std::uint16_t sop = swapEndian(op);
			ofs.write(reinterpret_cast<const char*>(&sop),
					sizeof(sop));
		}
		std::cout << "Assembled " << argv[1] << " and created "
			  << "Chip-8 binary " << target << std::endl;

	} catch(const AsmException& ae) {
		std::cout << ae.what() << std::endl;
	} catch(const DeviceException& de) {
		std::cout << de.what() << std::endl;
	} catch(const LabelException& le) {
		std::cout << le.what() << std::endl;
	} catch(const LexerException& le) {
		std::cout << le.what() << std::endl;
	} catch(const PrepLexerException& ple) {
		std::cout << ple.what() << std::endl;
	}

	//and return with no errors
	return EXIT_SUCCESS;
}

//end of program
