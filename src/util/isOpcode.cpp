/*
 * isOpcode.cpp
 * Implements a function that returns whether a string matches an opcode
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "isOpcode.h"
#include <vector>

//isOpcode function - returns whether a string is an opcode
bool isOpcode(const std::string& str) {
	//create an vector of opcodes
	static std::vector<std::string> ops = {
		"CLS", 
		"RET", 
		"CALL", 
		"JUMP", 
		"JMPI",
		"SKIP", 
		"MOV",
		"OR",
		"AND",
		"XOR",
		"ADD",
		"SUB",
		"SHR",
		"SUBB",
		"SHL",
		"DRAW",
		"RAND",
		"WAIT",
		"SCHR",
		"BCD",
		"LD",
		"DMP"
	};

	//and calculate whether the argument string is in the vector
	for(auto op : ops) {
		if(op == str) {
			return true;
		}
	}

	return false;
}

//end of implementation
