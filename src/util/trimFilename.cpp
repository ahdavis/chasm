/*
 * trimFilename.cpp
 * Implements a function that trims the extension from a filename
 * Created on 5/9/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "trimFilename.h"
#include <cstdlib>

//trimFilename method - trims the extension from a filename
std::string trimFilename(const std::string& filename) {
	//get the index of the start of the extension
	std::size_t lastIndex = filename.find_last_of("."); 

	//make sure that the filename actually has an extension
	if(lastIndex == std::string::npos) {
		return filename;
	}

	//and return the trimmed filename
	return filename.substr(0, lastIndex);
}

//end of implementation
