/*
 * fileExists.cpp
 * Implements a function that returns whether a file exists at a path
 * Created on 5/9/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "fileExists.h"
#include <cstdlib>

//fileExists function - returns whether a file exists at a path
bool fileExists(const std::string& path) {
	//attempt to open the file
	FILE* f = fopen(path.c_str(), "r");

	//check to see if the opening was successful
	if(f == NULL) {
		//file does not exist
		return false;
	} else {
		//file exists
		fclose(f);
		return true;
	}
}

//end of implementation
