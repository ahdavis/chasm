/*
 * swapEndian.cpp
 * Implements a function that swaps endianness of a 16-bit value
 * Created on 5/9/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include
#include "swapEndian.h"

//swapEndian function - swaps the endianness of a 16-bit value
std::uint16_t swapEndian(std::uint16_t val) {
	std::uint16_t ret = 0; //will hold the swapped value

	//get pointers to the input and output values
	std::uint8_t* dst = (std::uint8_t*)&ret;
	std::uint8_t* src = (std::uint8_t*)&val;

	//swap the bytes
	dst[0] = src[1];
	dst[1] = src[0];

	//and return the swapped value
	return ret;
}

//end of implementation
