/*
 * LabelException.cpp
 * Implements an exception that is thrown when an invalid label is found
 * Created on 5/6/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <sstream>
#include "LabelException.h"

//class implementation

//constructor
LabelException::LabelException(const std::string& badLbl,
				int line, int col)
	: errMsg()
{
	//assemble the error message
	std::stringstream ss;
	ss << "(" << line << ":" << col << ") ";
	ss << "Unknown label " << badLbl;
	this->errMsg = ss.str();
}

//destructor
LabelException::~LabelException() {
	//no code needed
}

//copy constructor
LabelException::LabelException(const LabelException& le)
	: errMsg(le.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
LabelException& LabelException::operator=(const LabelException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* LabelException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
