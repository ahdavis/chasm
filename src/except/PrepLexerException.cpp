/*
 * PrepLexerException.cpp
 * Implements an exception that is thrown when the preprocessor
 * encounters an unknown character
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <sstream>
#include "PrepLexerException.h"

//class implementation

//constructor
PrepLexerException::PrepLexerException(char badChar)
	: errMsg() //init the field
{
	//and assemble the error message
	std::stringstream ss;
	ss << "Unknown character: ";
	ss << badChar;
	this->errMsg = ss.str();
}

//destructor
PrepLexerException::~PrepLexerException() {
	//no code needed
}

//copy constructor
PrepLexerException::PrepLexerException(const PrepLexerException& ple)
	: errMsg(ple.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
PrepLexerException& PrepLexerException::operator=(const PrepLexerException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* PrepLexerException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
