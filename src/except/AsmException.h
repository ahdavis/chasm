/*
 * AsmException.h
 * Declares an exception that is thrown when the assembler 
 * encounters an error
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <exception>
#include "../codegen/TokenType.h"

//class declaration
class AsmException final : public std::exception {
	//public fields and methods
	public:
		//constructor
		AsmException(TokenType expected, TokenType found,
				int line, int col);

		//destructor
		~AsmException();

		//copy constructor
		AsmException(const AsmException& ae);

		//assignment operator
		AsmException& operator=(const AsmException& src);

		//called when the exception is thrown
		const char* what() const throw() override;

	//private fields and methods
	private:
		//field
		std::string errMsg; //the error message
};

//end of file
