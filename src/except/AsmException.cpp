/*
 * AsmException.cpp
 * Implements an exception that is thrown when the assembler 
 * encounters an error
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "AsmException.h"
#include <sstream>

//class implementation

//constructor
AsmException::AsmException(TokenType expected, TokenType found,
				int line, int col)
	: errMsg() //init the field
{
	//and assemble the error message
	std::stringstream ss;
	ss << "(" << line << ":" << col << ") ";
	ss << "Syntax error (expected ";
	ss << nameForTType(expected) << ", ";
	ss << "found " << nameForTType(found) << ")";
	this->errMsg = ss.str();
}

//destructor
AsmException::~AsmException() {
	//no code needed
}

//copy constructor
AsmException::AsmException(const AsmException& ae)
	: errMsg(ae.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
AsmException& AsmException::operator=(const AsmException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* AsmException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
