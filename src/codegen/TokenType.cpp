/*
 * TokenType.cpp
 * Implements a function for getting the name of a token type
 * Created on 5/2/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include
#include "TokenType.h"

//nameForTType function - returns the name of a TokenType
std::string nameForTType(TokenType type) {
	switch(type) {
		case TokenType::Opcode: {
			return "Opcode";
		}

		case TokenType::Label: {
			return "Label";
		}

		case TokenType::LblDef: {
			return "Label Definition";
		}

		case TokenType::Register: {
			return "Register";
		}

		case TokenType::Integer: {
			return "Integer";
		}

		case TokenType::Binary: {
			return "Binary";
		}

		case TokenType::SkipType: {
			return "SkipType";
		}

		case TokenType::Comma: {
			return "Comma";
		}

		case TokenType::Period: {
			return "Period";
		}

		case TokenType::Device: {
			return "Device";
		}

		default: {
			return "Unknown token type";
		}
	}
}

//end of implementation
