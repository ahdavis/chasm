/*
 * Token.h
 * Declares a class that represents an assembler token
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include <string>
#include "TokenType.h"

//class declaration
class Token {
	//public fields and methods
	public:
		//constructs from only a token type
		explicit Token(TokenType newType);

		//constructs from a type and a value
		Token(TokenType newType, const std::string& newValue);

		//destructor
		virtual ~Token();

		//copy constructor
		Token(const Token& t);

		//assignment operator
		Token& operator=(const Token& src);

		//getter methods
		
		//returns the type of the Token
		TokenType getType() const;

		//returns the value of the Token
		const std::string& getValue() const;

	//protected fields and methods
	protected:
		//fields
		TokenType type; //the type of the Token
		std::string value; //the value of the Token
};

//end of header
