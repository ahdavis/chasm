/*
 * Lexer.cpp
 * Implements a class that lexes assembly code
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include "Lexer.h"
#include "TokenType.h"
#include "../except/LexerException.h"
#include <cstdlib>
#include <sstream>
#include <iostream>

//class implementation

//constructor
Lexer::Lexer(const std::string& newText)
	: pos(0), text(newText), curChar('\0'), line(1), column(1)
{
	//init the current character
	this->curChar = this->text[this->pos];
}

//destructor
Lexer::~Lexer() {
	//no code needed
}

//copy constructor
Lexer::Lexer(const Lexer& l)
	: pos(l.pos), text(l.text), curChar(l.curChar), line(l.line),
		column(l.column)
{
	//no code needed
}

//assignment operator
Lexer& Lexer::operator=(const Lexer& src) {
	this->pos = src.pos; //assign the position
	this->text = src.text; //assign the text
	this->curChar = src.curChar; //assign the current character
	this->line = src.line; //assign the current line
	this->column = src.column; //assign the current column
	return *this; //and return the instance
}

//getLine method - returns the current line number
int Lexer::getLine() const {
	return this->line;
}

//getColumn method - returns the current column number
int Lexer::getColumn() const {
	return this->column;
}

//getNextToken method - returns the next token consumed from the text
Token Lexer::getNextToken() {
	//loop and generate tokens
	while(this->curChar != '\0') {
		//handle whitespace
		if(std::isspace(this->curChar)) {
			this->skipWhitespace();
			continue;
		}

		//handle comments
		if(this->curChar == ';') {
			this->consumeComment();
			continue;
		}

		//handle register references
		if((this->curChar == 'V') ||
			(this->curChar == 'I')) {
			return Token(TokenType::Register,
					this->regRef());
		}
		
		//handle labels
		if(this->curChar == '_') {
			//get the label text
			std::string lbl = this->label();

			//check to see if it is a definition
			char last = lbl.at(lbl.length() - 1);
			if(last == ':') {
				return Token(TokenType::LblDef,
						lbl.substr(0, 
							lbl.length() - 1));
			}

			return Token(TokenType::Label, lbl);
		}

		//handle opcodes
		if(std::isalpha(this->curChar)) {
			//get the opcode string
			std::string op = this->opcode();

			//check it to see if it refers to
			//a timing device or a skiptype
			if((op == "DELAY") || (op == "SOUND")) {
				//timing device
				return Token(TokenType::Device, op);
			} else if(((op == "EQ") || (op == "NE"))
					|| ((op == "KEY") 
						|| (op == "NOKEY"))) {
				//skiptype
				return Token(TokenType::SkipType, op);
			} else { //opcode
				return Token(TokenType::Opcode, op);
			}
		}

		//handle integer literals
		if(this->curChar == '$') {
			return Token(TokenType::Integer,
					this->integer());
		}

		//handle binary literals
		if(this->curChar == '%') {
			return Token(TokenType::Binary,
					this->binary());
		}

		//handle commas
		if(this->curChar == ',') {
			this->advance();
			return Token(TokenType::Comma, ",");
		}

		//handle periods
		if(this->curChar == '.') {
			this->advance();
			return Token(TokenType::Period, ".");
		}

		//if control reaches here, then no matches were found
		//so we throw an exception
		throw LexerException(this->curChar, this->line, 
					this->column);
	}

	//and return an EOF token
	return Token(TokenType::EndOfFile);
}

//private advance method - advances the lexer to the next character
void Lexer::advance() {
	this->pos++;
	if(this->pos > (this->text.length() - 1)) {
		this->curChar = '\0';
	} else {
		this->curChar = this->text[this->pos];
	}
}

//private skipWhitespace method - skips whitespace in the text
void Lexer::skipWhitespace() {
	//check for newlines
	if((this->curChar == '\n') || (this->curChar == '\r')) {
		this->advance();
		this->line++;
		this->column = 1;
	} else { //no newline found
		while(std::isspace(this->curChar) && 
			(this->curChar != '\0')) {
			this->advance();
		}
	}
}

//private consumeComment method - skips over a comment
void Lexer::consumeComment() {
	this->advance(); //advance past the semicolon
	while((this->curChar != '\n') && (this->curChar != '\r')
		&& (this->curChar != '\0')) {
		this->advance();
	}
}

//private opcode method - returns an opcode-like symbol from the text
std::string Lexer::opcode() {
	//declare the return symbol
	std::string ret;

	//loop and collect the symbol
	while(std::isalpha(this->curChar) &&
			(this->curChar != '\0')) {
		ret += std::toupper(this->curChar);
		this->advance();
	}

	//and return the symbol
	return ret;
}

//private label method - returns a label from the text
std::string Lexer::label() {
	//declare the return value
	std::string ret = "_";

	//advance to the first letter
	this->advance();

	//loop and collect the label
	while(std::isalpha(this->curChar) &&
			(this->curChar != '\0')) {
		ret += this->curChar;
		this->advance();
	}

	//check for a colon
	if(this->curChar == ':') {
		ret += ':';
		this->advance();
	}

	//and return the collected label
	return ret;
}

//private regRef method - returns a register reference from the text
std::string Lexer::regRef() {
	if(this->curChar == 'I') {
		this->advance();
		return "I";
	} else {
		std::string ret = "V";
		this->advance();
		ret += this->curChar;
		this->advance();
		return ret;
	}
}

//private integer method - returns an integer from the text
std::string Lexer::integer() {
	this->advance(); //advance past the dollar sign
	
	//loop and generate the integer string
	std::string buf;
	while(std::isxdigit(this->curChar) 
		&& (this->curChar != '\0')) {
		buf += this->curChar;
		this->advance();
	}

	//pad the string with zeroes
	std::string ret;
	std::size_t bufLen = buf.length();
	int numZeroes = 4 - bufLen;
	for(int i = 0; i < numZeroes; i++) {
		ret += '0';
	}
	ret += buf;

	//and return the integer string
	return ret;
}

//private binary method - returns a binary literal from the text
std::string Lexer::binary() {
	this->advance(); //advance past the percent sign

	std::string ret; //will hold the binary string

	//loop and generate the binary string
	for(int i = 0; i < 8; i++) {
		if((this->curChar != '0') && (this->curChar != '1')) {
			break;
		}
		ret += this->curChar;
		this->advance();
	}

	//and return the binary string
	return ret;
}

//end of implementation
