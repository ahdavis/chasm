/*
 * TokenType.h
 * Enumerates types of assembly tokens
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//include
#include <string>

//enum definition
enum class TokenType {
	Opcode,   //assembly instruction
	Label,    //address label
	LblDef,   //address label definition
	Register, //register reference
	Integer,  //integer literal
	Binary,   //binary literal
	SkipType, //skip instruction qualifier
	Comma,    //literal comma
	Period,   //literal period
	Device,   //timing device reference
	EndOfFile //end of source file
};

//function declaration
std::string nameForTType(TokenType type); //gets the name of a token type

//end of header
