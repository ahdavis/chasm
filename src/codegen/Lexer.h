/*
 * Lexer.h
 * Declares a class that lexes assembly code
 * Created on 5/1/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "Token.h"
#include <string>

//class declaration
class Lexer {
	//public fields and methods
	public:
		//constructor
		explicit Lexer(const std::string& newText);

		//destructor
		virtual ~Lexer();

		//copy constructor
		Lexer(const Lexer& l);

		//assignment operator
		Lexer& operator=(const Lexer& src);

		//getter methods
		
		//returns the current line being lexed
		int getLine() const;

		//returns the current column being lexed
		int getColumn() const;

		//returns the next Token consumed from the text
		Token getNextToken();

	//protected fields and methods
	protected:
		//methods
		void advance(); //advances the lexer to the next character
		void skipWhitespace(); //skips whitespace in the text
		void consumeComment(); //skips over a comment
		std::string opcode(); //returns an opcode from the text
		std::string label(); //returns a label from the text
		std::string regRef(); //returns a register reference
		std::string integer(); //returns an integer from the text
		std::string binary(); //returns binary from the text

		//fields
		unsigned int pos; //the current position in the text
		std::string text; //the text being lexed
		char curChar; //the current character being processed
		int line; //the current line being processed
		int column; //the current column being processed
};

//end of header
