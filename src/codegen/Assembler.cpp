/*
 * Assembler.cpp
 * Implements a class that assembles Chip-8 ROMs
 * Created on 5/6/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//includes
#include <sstream>
#include <iomanip>
#include <bitset>
#include "Assembler.h"
#include "../except/AsmException.h"
#include "../except/DeviceException.h"
#include "../except/LabelException.h"

//class implementation

//constructor
Assembler::Assembler(const Lexer& newLexer, 
			const AddressTable& newAddrTable)
	: lexer(newLexer), addrTable(newAddrTable),
		curToken(TokenType::EndOfFile), code()
{
	//get the first token from the lexer
	this->curToken = this->lexer.getNextToken();
}

//destructor
Assembler::~Assembler() {
	//no code needed
}

//copy constructor
Assembler::Assembler(const Assembler& a)
	: lexer(a.lexer), addrTable(a.addrTable), 
		curToken(a.curToken), code(a.code)
{
	//no code needed
}

//assignment operator
Assembler& Assembler::operator=(const Assembler& src) {
	this->lexer = src.lexer; //assign the lexer
	this->addrTable = src.addrTable; //assign the address table
	this->curToken = src.curToken; //assign the current token
	this->code = src.code; //assign the assembled code
	return *this; //and return the instance
}

//assemble method - assembles a Chip-8 program and returns 
//a vector of assembled machine code
const std::vector<std::uint16_t>& Assembler::assemble() {
	//loop and generate code
	while(true) {
		//check for an EOF token
		if(this->curToken.getType() == TokenType::EndOfFile) {
			break;
		}

		//handle different tokens
		if(this->curToken.getType() == TokenType::Opcode) {
			//process the opcode
			Opcode op = this->opcode();

			//add it to the code vector
			this->code.push_back(op.getValue());
		} else if(this->curToken.getType() == TokenType::Binary) {
			//get the first byte
			std::string b1 = this->binary();

			//correct the byte if it's zeroes
			if(b1 == "0") {
				b1 = "00";
			}

			//create a stringstream to process the binary
			std::stringstream ss;

			//process multiple binary literals
			if(this->curToken.getType() == TokenType::Binary) {
				//get the next byte
				std::string b2 = this->binary();

				//correct the byte if it's zeroes
				if(b2 == "0") {
					b2 = "00";
				}

				//and assemble the integer
				ss << std::hex << b1 << b2;
			} else {
				ss << std::hex << b1 << "00";
			}

			//retrieve the word from the stream
			std::uint16_t w;
			ss >> w;
			this->code.push_back(w);
		} else if(this->curToken.getType() == TokenType::Integer) {
			//get the integer string
			std::string is = this->integer();

			//convert it to a word
			std::stringstream ss;
			std::uint16_t i;
			ss << is;
			ss >> i;
			
			//and add it to the code
			this->code.push_back(i);
		} else if(this->curToken.getType() == TokenType::LblDef) {
			this->eat(TokenType::LblDef);
		}
	}

	//and return the code
	return this->code;	
}

//private eat method - verifies the current token and gets the next token
void Assembler::eat(TokenType type) {
	if(this->curToken.getType() == type) {
		this->curToken = this->lexer.getNextToken();
	} else {
		throw AsmException(type, this->curToken.getType(),
					this->lexer.getLine(),
					this->lexer.getColumn());
	}
}

//private opcode method - assembles an opcode
Opcode Assembler::opcode() {
	Token opToken = this->curToken; //save the current token
	this->eat(TokenType::Opcode); //get the next token
	
	//declare the return value
	Opcode ret;

	//handle the different opcodes
	if(opToken.getValue() == "CLS") { //00E0
		ret.appendNibble('0');
		ret.appendNibble('0');
		ret.appendNibble('E');
		ret.appendNibble('0');
	} else if(opToken.getValue() == "RET") { //00EE
		ret.appendNibble('0');
		ret.appendNibble('0');
		ret.appendNibble('E');
		ret.appendNibble('E');
	} else if(opToken.getValue() == "JUMP") { //1NNN
		//get the label to jump to
		std::string lbl = this->label(); 

		//append the first nibble of the opcode
		ret.appendNibble('1');

		//and loop and append the label
		for(int i = 1; i < 4; i++) {
			ret.appendNibble(lbl.at(i));
		}
	} else if(opToken.getValue() == "CALL") { //2NNN
		//get the label to call
		std::string lbl = this->label();

		//append the first nibble of the opcode
		ret.appendNibble('2');

		//and loop and append the label
		for(int i = 1; i < 4; i++) {
			ret.appendNibble(lbl.at(i));
		}
	} else if(opToken.getValue() == "JMPI") { //BNNN
		//get the label to index to
		std::string lbl = this->label();

		//append the first nibble of the opcode
		ret.appendNibble('B');

		//and loop and append the label
		for(int i = 1; i < 4; i++) {
			ret.appendNibble(lbl.at(i));
		}
	} else if(opToken.getValue() == "SKIP") { 
		this->eat(TokenType::Period); //parse the period
		std::string st = this->skipType(); //get the skiptype
		char reg = this->regRef(); //get the comparator register

		//handle different skips
		if(st == "EQ") {
			this->eat(TokenType::Comma);
			if(this->curToken.getType() ==
				TokenType::Integer) {
				//get the byte
				std::string bs = this->integer();

				//append the first nibble of the opcode
				ret.appendNibble('3');

				//append the register ID
				ret.appendNibble(reg);

				//and loop and append the byte
				for(int i = 2; i < 4; i++) {
					ret.appendNibble(bs.at(i));
				}
			} else if(this->curToken.getType() ==
					TokenType::Register) {
				//get the register ID
				char oreg = this->regRef();

				//append the first nibble of the opcode
				ret.appendNibble('5');

				//append the register IDs
				ret.appendNibble(reg);
				ret.appendNibble(oreg);

				//and append the last nibble of the opcode
				ret.appendNibble('0');
			}
		} else if(st == "NE") {
			this->eat(TokenType::Comma);
			if(this->curToken.getType() ==
				TokenType::Integer) {
				//get the byte
				std::string bs = this->integer();

				//append the first nibble of the opcode
				ret.appendNibble('4');

				//append the register ID
				ret.appendNibble(reg);

				//and loop and append the byte
				for(int i = 2; i < 4; i++) {
					ret.appendNibble(bs.at(i));
				}
			} else if(this->curToken.getType() ==
					TokenType::Register) {
				//get the register ID
				char oreg = this->regRef();

				//append the first nibble of the opcode
				ret.appendNibble('9');

				//append the register IDs
				ret.appendNibble(reg);
				ret.appendNibble(oreg);

				//and append the last nibble of the opcode
				ret.appendNibble('0');
			}

		} else if(st == "KEY") {
			//append the first nibble of the opcode
			ret.appendNibble('E');

			//append the register nibble
			ret.appendNibble(reg);

			//and append the remaining nibbles
			ret.appendNibble('9');
			ret.appendNibble('E');
		} else if(st == "NOKEY") {
			//append the first nibble
			ret.appendNibble('E');

			//append the register nibble
			ret.appendNibble(reg);

			//and append the remaining nibbles
			ret.appendNibble('A');
			ret.appendNibble('1');
		}
	} else if(opToken.getValue() == "MOV") {
		//handle different move targets
		if(this->curToken.getType() == TokenType::Register) {
			char tr = this->regRef(); //get the target register
			this->eat(TokenType::Comma); //eat the comma

			//handle different source objects
			if(this->curToken.getType() 
				== TokenType::Integer) {
				//get the integer string
				std::string iv = this->integer();

				//handle different destination objects
				if(tr == 'I') { //index register
					ret.appendNibble('A');
					for(int i = 1; i < 4; i++) {
						ret.appendNibble(
							iv.at(i));
					}
				} else { //normal register
					ret.appendNibble('6');
					ret.appendNibble(tr);
					for(int i = 2; i < 4; i++) {
						ret.appendNibble(
							iv.at(i));
					}
				}
			} else if(this->curToken.getType() 
					== TokenType::Register) {
				char sr = this->regRef();
				ret.appendNibble('8');
				ret.appendNibble(tr);
				ret.appendNibble(sr);
				ret.appendNibble('0');
			} else if(this->curToken.getType()
					== TokenType::Device) {
				//get the device reference
				std::string dev = this->device();

				//make sure that the device is not
				//the sound timer
				if(dev == "SOUND") {
					throw DeviceException(
						this->lexer.getLine(),
						this->lexer.getColumn());
				}

				//and generate the opcode
				ret.appendNibble('F');
				ret.appendNibble(tr);
				ret.appendNibble('0');
				ret.appendNibble('7');
			} else if((this->curToken.getType() ==
					TokenType::Label) && (tr == 'I')) {
				//get the label to load
				std::string lbl = this->label();

				//append the first nibble of the opcode
				ret.appendNibble('A');

				//loop and append the label
				for(int i = 1; i < 4; i++) {
					ret.appendNibble(lbl.at(i));
				}
			}
		} else if(this->curToken.getType() == TokenType::Device) {
			//get the target device
			std::string td = this->device();

			//parse the comma
			this->eat(TokenType::Comma);

			//get the source register
			char sr = this->regRef();

			//append the first two nibbles
			ret.appendNibble('F');
			ret.appendNibble(sr);

			//append the third nibble
			ret.appendNibble('1');

			//and handle different devices
			if(td == "SOUND") {
				ret.appendNibble('8'); 
			} else {
				ret.appendNibble('5');
			}
		}
	} else if(opToken.getValue() == "OR") {
		//get the operand registers
		char xr = this->regRef();
		this->eat(TokenType::Comma);
		char yr = this->regRef();

		//and generate the opcode
		ret.appendNibble('8');
		ret.appendNibble(xr);
		ret.appendNibble(yr);
		ret.appendNibble('1');
	} else if(opToken.getValue() == "AND") {
		//get the operand registers
		char xr = this->regRef();
		this->eat(TokenType::Comma);
		char yr = this->regRef();

		//and generate the opcode
		ret.appendNibble('8');
		ret.appendNibble(xr);
		ret.appendNibble(yr);
		ret.appendNibble('2');
	} else if(opToken.getValue() == "XOR") {
		//get the operand registers
		char xr = this->regRef();
		this->eat(TokenType::Comma);
		char yr = this->regRef();

		//and generate the opcode
		ret.appendNibble('8');
		ret.appendNibble(xr);
		ret.appendNibble(yr);
		ret.appendNibble('3');
	} else if(opToken.getValue() == "ADD") {
		//get the operand registers
		char xr = this->regRef();
		this->eat(TokenType::Comma);
		char yr = this->regRef();

		//and generate the opcode
		if(xr == 'I') {
			ret.appendNibble('F');
			ret.appendNibble(yr);
			ret.appendNibble('1');
			ret.appendNibble('E');
		} else {
			ret.appendNibble('8');
			ret.appendNibble(xr);
			ret.appendNibble(yr);
			ret.appendNibble('4');
		}
	} else if(opToken.getValue() == "SUB") {
		//get the operand registers
		char xr = this->regRef();
		this->eat(TokenType::Comma);
		char yr = this->regRef();

		//and generate the opcode
		ret.appendNibble('8');
		ret.appendNibble(xr);
		ret.appendNibble(yr);
		ret.appendNibble('5');
	} else if(opToken.getValue() == "SHR") {
		//get the operand register
		char xr = this->regRef();

		//and generate the opcode
		ret.appendNibble('8');
		ret.appendNibble(xr);
		ret.appendNibble('0');
		ret.appendNibble('6');
	} else if(opToken.getValue() == "SUBB") {
		//get the operand registers
		char xr = this->regRef();
		this->eat(TokenType::Comma);
		char yr = this->regRef();

		//and generate the opcode
		ret.appendNibble('8');
		ret.appendNibble(xr);
		ret.appendNibble(yr);
		ret.appendNibble('7');
	} else if(opToken.getValue() == "SHL") {
		//get the operand registers
		char xr = this->regRef();

		//and generate the opcode
		ret.appendNibble('8');
		ret.appendNibble(xr);
		ret.appendNibble('0');
		ret.appendNibble('E');
	} else if(opToken.getValue() == "DRAW") {
		//get the x and y registers
		char xr = this->regRef();
		this->eat(TokenType::Comma);
		char yr = this->regRef();
		this->eat(TokenType::Comma);

		//get the height value
		std::string n = this->integer();

		//and assemble the opcode
		ret.appendNibble('D');
		ret.appendNibble(xr);
		ret.appendNibble(yr);
		ret.appendNibble(n.at(3));
	} else if(opToken.getValue() == "RAND") {
		//get the operand register
		char tr = this->regRef();

		//parse the comma
		this->eat(TokenType::Comma);

		//get the value to AND with the random number
		std::string av = this->integer();

		//and generate the opcode
		ret.appendNibble('C');
		ret.appendNibble(tr);
		ret.appendNibble(av.at(2));
		ret.appendNibble(av.at(3));
	} else if(opToken.getValue() == "WAIT") {
		//get the operand register
		char tr = this->regRef();

		//and assemble the opcode
		ret.appendNibble('F');
		ret.appendNibble(tr);
		ret.appendNibble('0');
		ret.appendNibble('A');
	} else if(opToken.getValue() == "SCHR") {
		//get the operand register
		char sr = this->regRef();

		//and assemble the opcode
		ret.appendNibble('F');
		ret.appendNibble(sr);
		ret.appendNibble('2');
		ret.appendNibble('9');
	} else if(opToken.getValue() == "BCD") {
		//get the operand register
		char sr = this->regRef();

		//and assemble the opcode
		ret.appendNibble('F');
		ret.appendNibble(sr);
		ret.appendNibble('3');
		ret.appendNibble('3');
	} else if(opToken.getValue() == "DMP") {
		//get the limit register
		char lr = this->regRef();

		//and generate the opcode
		ret.appendNibble('F');
		ret.appendNibble(lr);
		ret.appendNibble('5');
		ret.appendNibble('5');
	} else if(opToken.getValue() == "LD") {
		//get the limit register
		char lr = this->regRef();

		//and generate the opcode
		ret.appendNibble('F');
		ret.appendNibble(lr);
		ret.appendNibble('6');
		ret.appendNibble('5');
	}

	//and return the generated opcode
	return ret;
}

//private label method - assembles a label
std::string Assembler::label() {
	//prepare to get the label
	Token lblToken = this->curToken; //save the current token
	this->eat(TokenType::Label); //parse the label token

	//get the text label
	std::string tlbl = lblToken.getValue();

	//make sure it exists
	if(!this->addrTable.hasEntry(tlbl)) {
		throw LabelException(tlbl, this->lexer.getLine(),
					this->lexer.getColumn());
	}

	//get the address from the lookup table
	std::uint16_t addr = this->addrTable.getEntry(tlbl);

	//mask it
	addr &= 0x0FFF;

	//convert it to a string
	std::stringstream ss;
	ss << std::hex << std::setfill('0') << std::setw(4) << addr;
	std::string hexlbl = ss.str();

	//and return the string
	return hexlbl;
}

//private regRef method - assembles a register reference
char Assembler::regRef() {
	Token regToken = this->curToken; //save the token
	this->eat(TokenType::Register); //parse the register
	if(regToken.getValue().at(0) == 'I') {
		return 'I';
	} else {
		return regToken.getValue().at(1);
	}
}

//private skipType method - assembles a skip type
std::string Assembler::skipType() {
	Token skToken = this->curToken; //save the token
	this->eat(TokenType::SkipType); //parse the skip type
	return skToken.getValue(); //and return the skiptype string
}

//private integer method - assembles an integer literal
std::string Assembler::integer() {
	Token iToken = this->curToken; //save the token
	this->eat(TokenType::Integer); //parse the integer

	//generate the string
	std::stringstream ss;
	ss << std::hex << std::setfill('0') << std::setw(4)
		       << iToken.getValue();
	std::string ret = ss.str();

	//and return it
	return ret;
}

//private binary method - assembles a binary literal
std::string Assembler::binary() {
	Token bToken = this->curToken; //save the token
	this->eat(TokenType::Binary); //parse the binary literal

	//generate the byte
	std::bitset<8> bits(bToken.getValue());
	std::uint8_t bb = (bits.to_ulong() & 0xFF);
	int b = static_cast<unsigned int>(bb);

	//convert it to a hex string
	std::stringstream ss;
	ss << std::hex << b;
	std::string bs = ss.str();

	//and return the byte
	return bs;
}

//private device method - assembles a device reference
std::string Assembler::device() {
	Token dToken = this->curToken; //save the token
	this->eat(TokenType::Device); //parse the device
	return dToken.getValue(); //and return the name of the device
}

//end of implementation
