/*
 * Assembler.h
 * Declares a class that assembles Chip-8 ROMs
 * Created on 5/6/2019
 * Created by Andrew Davis
 *
 * Copyright (C) 2019  Andrew Davis
 *
 * This program is free software: you can redistribute it and/or modify   
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//include guard
#pragma once

//includes
#include "Lexer.h"
#include "Token.h"
#include "TokenType.h"
#include "../prep/AddressTable.h"
#include "../opcode/Opcode.h"
#include <vector>
#include <cstdint>

//class declaration
class Assembler {
	//public fields and methods
	public:
		//constructor
		Assembler(const Lexer& newLexer,
				const AddressTable& newAddrTable);

		//destructor
		virtual ~Assembler();

		//copy constructor
		Assembler(const Assembler& a);

		//assignment operator
		Assembler& operator=(const Assembler& src);

		//assembles a Chip-8 program and returns
		//a vector of assembled machine code
		const std::vector<std::uint16_t>& assemble();

	//protected fields and methods
	protected:
		//methods
		void eat(TokenType type); //gets the next token
		Opcode opcode(); //assembles an opcode
		std::string label(); //assembles a label
		char regRef(); //assembles a register reference
		std::string skipType(); //assembles a skip type
		std::string integer(); //assembles an integer literal
		std::string binary(); //assembles a binary literal
		std::string device(); //assembles a device reference

		//fields
		Lexer lexer; //the symbol lexer 
		AddressTable addrTable; //the address table
		Token curToken; //the current token being assembled
		std::vector<std::uint16_t> code; //the assembled code
};

//end of header
