# Makefile for chasm
# Compiles the code for the assembler
# Created on 4/28/2019
# Created by Andrew Davis
#
# Copyright (C) 2019  Andrew Davis
#
# This program is free software: you can redistribute it and/or modify   
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# define the compiler
CXX=g++

# define the compiler flags
CXXFLAGS=-c -Wall -std=c++17

# define the linker flags
LDFLAGS=

# define state-specific compiler flags
debug: CXXFLAGS += -g

# retrieve the source code
MAIN=$(shell ls src/*.cpp)
OPCD=$(shell ls src/opcode/*.cpp)
EXCE=$(shell ls src/except/*.cpp)
PREP=$(shell ls src/prep/*.cpp)
CGEN=$(shell ls src/codegen/*.cpp)
UTIL=$(shell ls src/util/*.cpp)

# list the source code
SOURCES=$(MAIN) $(OPCD) $(EXCE) $(PREP) $(CGEN) $(UTIL)

# compile the source code
OBJECTS=$(SOURCES:.cpp=.o)

# define the executable name
EXECUTABLE=chasm

# start of build code

# target to compile the entire project without debug symbols
all: $(SOURCES) $(EXECUTABLE)

# target to build the executable without debug symbols
$(EXECUTABLE): $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $@ bin/
	mv -f $(OBJECTS) obj/

# target to build the executable with debug symbols
debug: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $(EXECUTABLE) $(LDFLAGS)
	mkdir bin
	mkdir obj
	mv -f $(EXECUTABLE) bin/
	mv -f $(OBJECTS) obj/

# target to compile source code to object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# target to clean the workspace
clean:
	rm -rf bin
	rm -rf obj

# target to install the compiled program
# REQUIRES ROOT
install:
	cp bin/$(EXECUTABLE) /usr/local/bin/

# end of Makefile
