# chasm

Open-source Chip-8 assembler

# Overview

`chasm` is an assembler for the Chip-8 emulator platform.
It allows for creation of Chip-8 software using an easy-to-learn
assembly language. This language is detailed below.

# Installation

Clone this repository to your machine and `cd` into its root directory.
Then, type `make` and then `sudo make install`. 

# Usage

To assemble a Chip-8 program, type `chasm <path>`, where `<path>` is
the path to the source file. If there are no errors in your code, 
a binary file with the extension `.c8` and the same name as your source
file will be created in the same directory as your source file.

# Assembler Instructions

`chasm` understands 22 assembler instructions. Here they are:

## `CLS`

This instruction takes no arguments and clears the display screen.

## `RET`

This instruction takes no arguments. It is used to return from
a subroutine that was executed by the `CALL` subroutine.

## `CALL label`

This instruction executes a subroutine located at the memory location
denoted by `label`. Execution continues from that address until a `RET`
instruction is found, at which point execution continues from the
instruction immediately following the `CALL` instruction.

## `JUMP label`

This instruction causes program execution to unconditionally 
jump to `label`.

## `JMPI index`

This instruction causes program execution to unconditonally jump to
the memory address equal to the `V0` register's value plus `index`.

## `SKIP.NE register1, (register2 | value)`

This instruction causes the next instruction to be skipped if `register1`
is not equal to `register2` or `value`, whichever is supplied.

## `SKIP.EQ register1, (register2 | value)`

This instruction causes the next instruction to be skipped if `register1`
is equal to `register2` or `value`, whichever is supplied.

## `SKIP.KEY register`

This instruction causes the next instruction to be skipped if the key
corresponding to `register` is pressed.

## `SKIP.NOKEY register`

This instruction causes the next instruction to be skipped if the key
corresponding to `register` is not pressed.

## `MOV dst, src`

This instruction copies the value of `src` into `dst`. The `dst` argument
can be a register or a device (see Devices below for more information).
The `src` argument can be a register, a constant, or the delay timer 
device (again, see Devices below for more information). Your program
will not assemble if `src` is the sound timer device.

## `OR register1, register2`

This instruction calculates the bitwise OR of `register1` and `register2`
and puts the result into `register1`.

## `AND register1, register2`

This instruction calculates the bitwise AND of `register1` and `register2`
and puts the result into `register1`.

## `XOR register1, register2`

This instruction calculates the bitwise XOR of `register1` and `register2`
and puts the result into `register1`.

## `ADD register1, (register2 | value)`

This instruction adds its second argument to its first argument and puts
the result in its first argument. Note that `register2` cannot be the index
register. If `register1` is not the index register and the second argument
is a register, the `VF` register will be set to 1 if there is a carry
as a result of the addition and to 0 if not.

## `SUB register1, register2`

This instruction calculates the difference of `register1` and `register2`
and puts the result in `register1`. Note that neither register can be
the index register. If there is a borrow involved in the subtraction,
the `VF` register will be set to 0. If not, the `VF` register will be set
to 1.

## `SHR register`

This instruction puts the least significant bit of `register` into the
`VF` register and bitwise shifts `register` to the right by 1.

## `SUBB register1, register2`

This instruction is the same as `SUB`, except that `register1` is
subtracted from `register2`.

## `SHL register`

This instruction puts the most significant bit of `register` into the
`VF` register and bitwise shifts `register` to the left by 1.

## `DRAW xRegister, yRegister, h`

This instruction draws a sprite 8 pixels wide and `h` pixels tall at
(`xRegister`, `yRegister`). The sprite is read from memory starting at
the address currently stored in the `I` register.

## `RAND register, value`

This instruction generates a random integer between 0 and 255 (inclusive),
bitwise ANDs it with `value`, and stores the result in `register`.

## `WAIT register`

This instruction stops execution until a valid key is pressed, and then
stores the index of that key in `register`.

## `SCHR register`

This instruction stores the address of the character stored in `register`
in the `I` register.

## `BCD register`

This instruction stores the binary-coded decimal representation of
`register` in memory starting at the address stored in the `I` register.

## `LD register`

This instruction loads sequential bytes from memory starting at the address
stored in the `I` register and stores them into registers ranging from
`V0` to `register` inclusive.

## `DMP register`

This instruction stores the values from `V0` through `register` inclusive
into memory, starting at the address stored in the `I` register.

# Devices

The Chip-8 processor has two timing devices. Each device counts down to
zero at a rate of one tick per second. The first device is called the
sound timer. If its value is not zero, a tone will sound each second. To
refer to it in your code, use the literal `SOUND`. The second device is
called the delay timer. It is the only timing device capable of being both
read from and written to. To refer to it in your code, use the literal
`DELAY`.

# Registers

The Chip-8 processor has sixteen general-purpose registers and one index
register. The general-purpose registers are indexed as `V0` through `VF`,
and can each hold one byte. The index register is named `I`, and can hold
two bytes.

# Memory

The Chip-8 platform has 4096 bytes of available memory. Programs are loaded
into memory starting at address `0x0200`, and the native fontset is stored
in memory ranging from `0x0000` through `0x0050`. All other memory is
available for whatever purpose you desire.

# Literals

To define numeric literals in your code, you have two options. The first
is to use a hexadecimal literal. A hexadecimal literal always begins with
a dollar sign and can contain one to three digits. Your other option is to
use a binary literal. A binary literal begins with a percent sign and
always contains eight digits. Binary literals are very useful for defining
sprites. To do this, define a label (see next section) and draw out your
sprite in binary after the label. Then, make the `I` register point to
your label and use the `DRAW` instruction to display your sprite.

# Labels

To create easy-to-access memory locations and subroutines, you can use
labels. Label definitions begin with an underscore and end with a colon.
To reference a defined label, such as with the `CALL` instruction, use
the name of the label without a colon at the end.

# License

`chasm` is licensed under the GNU General Public License, version 3. See
the `LICENSE` file for the full text of the license.

# Contributing to `chasm`

If you feel you can contribute to `chasm`, feel free to submit a
pull request. If you find a bug or another issue, please open an issue.


Thanks for using `chasm`!
